import "./App.css";
import NavigationBar from "./Components/NavigationBar";
import Notification from "./Components/Notification";
import MyNetworkContainer from "./Components/MyNetwork/MyNetworkContainer";
import { Routes, Route } from "react-router-dom";
import User from "./Components/User/User";
import Job from "./Components/Job/Job";
import Home from "./Components/Home/Home";
import Message from "./Components/Message/Message";
import News from "./Components/News/News";

function App() {
  return (
    <div className="App">
      <NavigationBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/notification" element={<Notification />} />
        <Route path="/mynetwork" element={<MyNetworkContainer />}></Route>
        <Route path="/user/:userdata" element={<User />}></Route>
        <Route path="/job" element={<Job />}></Route>
        <Route path="/message" element={<Message />}></Route>
        <Route path="/news" element={<News/>}></Route>
      </Routes>
    </div>
  );
}
export default App;
