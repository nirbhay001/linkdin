import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
} from "./MyNetworkType";
const initialState = {
  status: "loading",
  networks: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        status: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        status: "loaded",
        networks: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        status: "error",
        networks: [],
      };
    default:
      return state;
  }
};

export default userReducer;
