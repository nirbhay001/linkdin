import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
} from "./MessageType";

const initialState = {
  status: "loading",
  messages: [],
};

const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        status: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        status: "loaded",
        messages: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        status: "error",
        messages: [],
      };
    default:
      return state;
  }
};

export default messageReducer;
