import axios from "axios";
import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
} from "./NotificationType";
export const filterData = "filterData";

export const fetchUserRequest = () => {
  return {
    type: FETCH_USER_REQUEST,
  };
};
export const fetchUserSuccess = (users) => {
  return {
    type: FETCH_USER_SUCCESS,
    payload: users,
  };
};
export const fetchUserFailure = () => {
  return {
    type: FETCH_USER_FAILURE,
    payload: "error",
  };
};

export const fetchnotifications = () => {
  return (dispatch) => {
    dispatch(fetchUserRequest());
    axios
      .get("https://linkedin-api-eight.vercel.app/api/notifications/")
      .then((response) => {
        const users = response.data;
        dispatch(fetchUserSuccess(users));
      })
      .catch((error) => {
        const errorMessage = error.message;
        dispatch(fetchUserFailure(errorMessage));
      });
  };
};
