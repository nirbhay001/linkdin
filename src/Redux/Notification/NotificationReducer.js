import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
} from "./NotificationType";

const initialState = {
  status: "loading",
  notifications: [],
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        status: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        status: "loaded",
        notifications: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        status: "error",
        notifications: [],
      };
    default:
      return state;
  }
};

export default notificationReducer;
