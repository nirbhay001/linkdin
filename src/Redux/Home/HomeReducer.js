import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  FILTER_DATA,
  POST_DATA_SUCCESS,
  POST_DATA_FAILURE,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILURE,
} from "./HomeType.js";

let initialState = {
  status: "loading",
  users: [],
  filterData: [],
  payload: "",
  error: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        status: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        status: "loaded",
        users: action.payload,
        filterData: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        ...state,
        status: "error",
        users: [],
      };

    case POST_DATA_SUCCESS:
      return {
        ...state,
        filterData: [...state.filterData, action.payload],
        error: null,
      };
    case POST_DATA_FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case UPDATE_DATA_SUCCESS:
      return {
        ...state,
        filterData: state.filterData.map((item) => {
          return item.id === action.payload.id ? action.payload : item;
        }),
      };
    case UPDATE_DATA_FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case FILTER_DATA:
      if (action.payload.length === 0) {
        return {
          ...state,
          filterData: state.users,
        };
      } else {
        let filterSearchData = state.users.filter((item) => {
          return (
            item.name.toLowerCase().indexOf(action.payload.toLowerCase()) !== -1
          );
        });
        return {
          ...state,
          filterData: filterSearchData,
        };
      }
    default:
      return state;
  }
};

export default userReducer;
