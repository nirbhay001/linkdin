import axios from "axios";
import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  POST_DATA_FAILURE,
  POST_DATA_SUCCESS,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILURE,
} from "./HomeType.js";
export const filterData = "filterData";

export const fetchUserRequest = () => {
  return {
    type: FETCH_USER_REQUEST,
  };
};
export const fetchUserSuccess = (users) => {
  return {
    type: FETCH_USER_SUCCESS,
    payload: users,
  };
};
export const fetchUserFailure = () => {
  return {
    type: FETCH_USER_FAILURE,
    payload: "error",
  };
};

export const postDataSuccess = (data) => {
  return {
    type: POST_DATA_SUCCESS,
    payload: data,
  };
};

export const postDataFailure = (error) => {
  return {
    type: POST_DATA_FAILURE,
    payload: error,
  };
};

export const updateDataSuccess = (data) => {
  return {
    type: UPDATE_DATA_SUCCESS,
    payload: data,
  };
};

export const updateDataFailure = (error) => {
  return {
    type: UPDATE_DATA_FAILURE,
    payload: error,
  };
};

export const fetchUsers = () => {
  return (dispatch) => {
    dispatch(fetchUserRequest());
    axios
      .get("https://linkedin-api-eight.vercel.app/api/users/")
      .then((response) => {
        const users = response.data;
        dispatch(fetchUserSuccess(users));
      })
      .catch((error) => {
        const errorMessage = error.message;
        dispatch(fetchUserFailure(errorMessage));
      });
  };
};

export const postData = (data) => {
  return (dispatch) => {
    axios
      .post("https://linkedin-api-eight.vercel.app/api/users/", data)
      .then((response) => {
        dispatch(postDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(postDataFailure(error.message));
      });
  };
};

export const updateData = (data) => {
  return (dispatch) => {
    let id = data.id;
    axios
      .put(`https://linkedin-api-eight.vercel.app/api/users/${id}`, data)
      .then((response) => {
        dispatch(updateDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(updateDataFailure(error.message));
      });
  };
};
