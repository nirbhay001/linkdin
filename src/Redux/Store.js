import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./Home/HomeReducer";
import { combineReducers } from "@reduxjs/toolkit";
import notificationReducer from "./Notification/NotificationReducer";
import MyNetworkReducer from "./MyNetwork/MyNetworkReducer";
import jobReducer from "./Job/JobReducer";
import messageReducer from "./Message/MessageReducer";

const rootReducer = combineReducers({
  user: userReducer,
  notification: notificationReducer,
  myNetwork: MyNetworkReducer,
  jobs: jobReducer,
  messages: messageReducer,
});

const store = configureStore({
  reducer: {
    rootReducer,
  },
});
export default store;
