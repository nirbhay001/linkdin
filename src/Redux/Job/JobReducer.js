import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
} from "./JobType";

const initialState = {
  status: "loading",
  jobs: [],
};

const jobReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        status: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        status: "loaded",
        jobs: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        status: "error",
        jobs: [],
      };
    default:
      return state;
  }
};

export default jobReducer;
