import { connect } from "react-redux";
import NetworkItem from "./NetworkItem";

function MyNetworkSuggestion(props) {
  return (
    <div
      className="d-flex justify-content-between align-items-center p-3"
      style={{ width: "50rem", flexWrap: "wrap" }}
    >
      {props.networkData.networks.map((item) => (
        <NetworkItem item={item} />
      ))}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    networkData: state.rootReducer.myNetwork,
  };
};

export default connect(mapStateToProps, null)(MyNetworkSuggestion);
