import React from "react";
import { useState } from "react";

function NetworkItem(props) {
  const [isFollowing, setIsFollowing] = useState(false);

  const handleFollowClick = () => {
    setIsFollowing(!isFollowing);
  };

  return (
    <div className="d-flex flex-row justify-content-between align-items-center mt-3 gap-2 hover-element">
      <div
        className="d-flex flex-row col-2 rounded-2 border border-2 border-rgb(102,102,102) align-items-center m-3  d-none d-lg-block "
        style={{ width: "220px", height: "20rem", position: "relative" }}
      >
        <div style={{ borderBottom: "4rem solid rgb(160,180,183)" }}></div>
        <div
          style={{
            position: "absolute",
            top: "2.5rem",
            left: "20%",
            transform: "translateX(-50%)",
          }}
        >
          <img
            src={props.item.imgUrl}
            className="rounded-circle  py-n4"
            style={{ width: "3rem", height: "3rem" }}
          />
        </div>
        <div className="d-flex flex-column gap-1 p-1 justify-content-start">
          <div className="font-weight-bold mt-5 text-lg">{props.item.name}</div>
          <span className="text-small pl-3 suggestion mt-1">
            SDE 2 @ {props.item.company}| 160k+ LinkedIn | Ex - Autodesk, TCS |
            {props.item.position}| Content Writer | Alexa Enthusiast
          </span>
          <button
            className={`btn btn-outline-${
              isFollowing ? "secondary" : "primary"
            } rounded-3 padding-3`}
            onClick={handleFollowClick}
          >
            {isFollowing ? "Following" : "Follow"}
          </button>
        </div>
      </div>
    </div>
  );
}

export default NetworkItem;
