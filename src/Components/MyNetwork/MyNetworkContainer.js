import React from 'react'
import MyNetwork from './MyNetwork'
import MyNetworkSideBar from './MyNetworkSideBar'
import MyNetworkSuggestion from './MyNetworkSuggestion'

export default function MyNetworkContainer() {
  return (
    <div className="d-flex justify-content-center " >
          <MyNetworkSideBar />
          <div className='d-flex flex-column ' style={{width:"50%"}}>
          <MyNetwork/>
          <div className='d-flex flex-wrap'>
          <MyNetworkSuggestion/>
          </div>
          </div>
    </div>
  )
}
