import React from "react";

function MyNetworkSideBar() {
  return (
    <div
      className="d-flex flex-column justify-content-start rounded-2 border border-2 border-rgb(102,102,102) w-50 m-3 p-3 d-none d-md-block network-sidebar"
      style={{ height: "60vh", width: "20rem", position: "relative" }}
    >
      <div style={{ textAlign: "left" }}>Manage My Network</div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-3">
          <i className="fa-solid fa-user-group fa-lg mx-sm-2"></i>
          <span>Connections</span>
        </div>
        <div>
          <span>93</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-address-book fa-lg mx-sm-2"></i>
          <span>Contacts</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-user-group fa-lg mx-sm-2"></i>
          <span>Following & followers</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-user fa-lg mx-sm-2"></i>
          <span>Group</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-calendar-days fa-lg mx-sm-2"></i>
          <span>Events</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-file fa-lg mx-sm-2"></i>
          <span>Pages</span>
        </div>
        <div>
          <span>52</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-newspaper fa-lg mx-sm-2"></i>
          <span>NewsLetter</span>
        </div>
        <div>
          <span>2</span>
        </div>
      </div>
      <div className="d-flex justify-content-between pt-4">
        <div className="d-flex justify-content-start align-items-center gap-2">
          <i className="fa-solid fa-hashtag fa-lg mx-sm-2"></i>
          <span>Hashtag</span>
        </div>
        <div>
          <span>1</span>
        </div>
      </div>
    </div>
  );
}

export default MyNetworkSideBar;
