import React from "react";
import { connect } from "react-redux";
import { myNetworkUsers } from "../../Redux/MyNetwork/MyNetworkAction";
import { useEffect, useState } from "react";
import { InfinitySpin } from "react-loader-spinner";

function MyNetwork(props) {
  const [acceptFlag, setAcceptFlag] = useState({
    flag: false,
    user: null,
  });

  useEffect(() => {
    props.myNetworkUsers();
  }, []);

  const acceptRequest = (userData) => {
    setAcceptFlag((prev) => {
      return {
        ...prev,
        flag: true,
        user: userData,
      };
    });
  };

  return (
    <>
      <div className=" d-flex flex-column ">
        <div className="container d-flex justify-content-center ">
          <div className="container d-flex flex-column  justify-contents-center rounded-2 border border-2 border-rgb(102,102,102) my-network  mt-3">
            <div className="d-flex justify-content-between p-3">
              <div>
                <span>Invitations</span>
              </div>
              <div>
                <span>See all</span>
              </div>
            </div>
            {acceptFlag.flag === true ? (
              <div className="ms-sm-3">
                invite accepted{" "}
                <span className="text-primary">{acceptFlag.user.name}</span>
              </div>
            ) : (
              ""
            )}
            <hr />
            {props.networkData.status === "loading" ? (
              <div className="text-center">
                <InfinitySpin width="200" color="#4fa94d" />
              </div>
            ) : props.networkData.status === "error" ? (
              <h1>Data has not been fetched</h1>
            ) : (
              props.networkData.networks.map((item) => {
                return (
                  <div
                    className="d-flex justify-content-between align-items-center mt-3 gap-2 hover-element"
                    style={{ width: "40rem" }}
                    key={item.id}
                  >
                    <div className="d-flex gap-2">
                      <img
                        src={item.imgUrl}
                        className="rounded-circle"
                        style={{ width: "5rem", height: "5rem" }}
                        alt="rounded image"
                      />
                      <div>
                        <span className="small d-flex flex-column align-items-start justify-content-start gap-1">
                          <span>News . Weekly</span>
                          <span className="d-flex flex-column">
                            <span>{item.name}</span>
                            <span>{item.company}</span>
                            <span>{item.position}</span>
                          </span>
                        </span>
                      </div>
                    </div>
                    <div className="d-flex justify-content-center gap-5">
                      <div>Ignore</div>
                      <button
                        type="button"
                        className="btn btn-outline-primary text-dark"
                        onClick={() => acceptRequest(item)}
                      >
                        Accept
                      </button>
                    </div>
                  </div>
                );
              })
            )}
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    networkData: state.rootReducer.myNetwork,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    myNetworkUsers: () => dispatch(myNetworkUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyNetwork);
