import React from "react";
import "./Message.css";
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { messageUsers } from "../../Redux/Message/MessageAction";
import { InfinitySpin } from "react-loader-spinner";

function Message(props) {
  const [state, setState] = useState({});

  useEffect(() => {
    props.messageUsers();
  }, []);

  return (
    <div className="d-flex justify-content-center col-8 mt-3  m-auto">
      <div className="d-flex justify-content-center border border-2 border-rgb(102,102,102) rounded-2 col-8">
        <div
          className=" d-flex flex-column rounded-2 col-2 align-items-start p-2 mt-3 border-right border-2 border-rgb(102,102,102)  d-none d-md-block "
          style={{ width: "300px", height: "50rem", position: "relative" }}
        >
          <div className="d-flex justify-content-between">
            <div>Messaging</div>
            <div className="d-flex gap-3">
              <div>
                <i class="fa-solid fa-pen-to-square fa-xl"></i>
              </div>
              <div>...</div>
            </div>
          </div>
          <hr />
          <div className="input-group mb-3">
            <span
              className="input-group-text bg-transparent text-black"
              id="basic-addon1"
            >
              <i className="fa-solid fa-magnifying-glass" />
            </span>
            <input
              type="text"
              className="form-control"
              placeholder="search message"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </div>
          <hr className="pl-4" />
          {props.messages.status === "loading" ? (
            <div className="text-center col-8">
              <InfinitySpin width="200" color="#4fa94d" />
            </div>
          ) : props.messages.status === "error" ? (
            <h1>Error in fetching the data</h1>
          ) : (
            props.messages.messages.map((item) => {
              return (
                <div className="d-flex gap-2 p-2 item-hover">
                  <img
                    src={item.imgUrl}
                    className="rounded-circle  py-n4 "
                    style={{ width: "4rem", height: "4rem" }}
                  />
                  <div
                    className="d-flex flex-column"
                    onClick={() => {
                      setState(item);
                    }}
                  >
                    <div className="text-bold">
                      <div>{item.name}</div>
                    </div>
                    {item.message === undefined ? (
                      ""
                    ) : (
                      <div>{item.message.slice(0, 20)}</div>
                    )}
                  </div>
                  <div>3 May</div>
                </div>
              );
            })
          )}
        </div>
        <div className="d-flex flex-column justify-content-between col border border-right border-1">
          <div>
            <div className="d-flex justify-content-between p-2">
              <div className="d-flex flex-column message-gap">
                <div>{state.name}</div>
                <div>{state.profession}</div>
              </div>
              <div>...</div>
            </div>
            <hr className="margin-hr" />
            <div className="d-flex flex-column ">
              <div className="d-flex flex-column p-3">
                <div className="d-flex gap-2">
                  <img
                    src={state.imgUrl}
                    className="rounded-circle  py-n4 "
                    style={{ width: "4rem", height: "4rem" }}
                  />
                  <div className="p-3 ">{state.name}</div>
                </div>
                <div className="p-3">{state.message}</div>
              </div>
            </div>
          </div>
          <div>
            <hr />
            <div className="form-floating">
              <textarea
                className="form-control mb-3"
                placeholder="Leave a comment here"
                id="floatingTextarea"
                defaultValue={""}
              />
              <label htmlFor="floatingTextarea">Write Message</label>
            </div>
            <div className="d-flex justify-content-between gap-3 p-3">
              <div className="d-flex gap-3">
                <i className="fa-sharp fa-solid fa-image fa-xl"></i>
                <i className="fa-solid fa-file fa-xl"></i>
              </div>
              <div className="d-flex gap-3">
                <div className="hover-element">send</div>
                <div>...</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    messages: state.rootReducer.messages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    messageUsers: () => dispatch(messageUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Message);
