import React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import messageUsers from "../../Redux/Message/MessageReducer";

function MessageSidebar(props) {
  useEffect(() => {
    props.messageUsers();
  }, []);

  return (
    <div
      className=" d-flex flex-column rounded-2 col-2 align-items-start p-2 mt-3 border-right border-2 border-rgb(102,102,102)  d-none d-md-block "
      style={{ width: "10rem", height: "50rem" }}
    >
      <div className="d-flex justify-content-between">
        <div>Messaging</div>
        <div className="d-flex gap-3">
          <div>
            <i class="fa-solid fa-pen-to-square fa-xl"></i>
          </div>
          <div>...</div>
        </div>
      </div>
      <hr />
      <div className="input-group mb-3">
        <span
          className="input-group-text bg-transparent text-black"
          id="basic-addon1"
        >
          <i className="fa-solid fa-magnifying-glass" />
        </span>
        <input
          type="text"
          className="form-control"
          placeholder="search message"
          aria-label="Username"
          aria-describedby="basic-addon1"
        />
      </div>
      <div className="d-flex gap-2 p-2">
        <img
          src={"https://placeimg.com/400/400/any"}
          className="rounded-circle  py-n4 "
          style={{ width: "5rem", height: "4rem" }}
        />
        <div className="d-flex flex-column">
          <div className="text-bold">
            <div>Akshay Kumar</div>
          </div>
          <div>this is the message given by customer...</div>
        </div>
        <div>3 May</div>
      </div>
      <hr className="pl-4" />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    messages: state.rootReducer.messages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    messageUsers: () => dispatch(messageUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageSidebar);
