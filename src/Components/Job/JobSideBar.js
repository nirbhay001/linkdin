import React from "react";

function JobSideBar() {
  return (
    <div
      className="d-flex flex-column rounded-2 border border-2 border-rgb(102,102,102)  m-3 p-3 d-none d-md-block "
      style={{ width: "200px", height: "40vh", position: "relative" }}
    >
      <div className="d-flex gap-3">
        <span>
          <i class="fa-sharp fa-solid fa-bookmark fa-lg"></i>
        </span>
        <span>My Jobs</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          <i className="fas fa-bell fa-lg" />
        </span>
        <span>Job Alert</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          <i class="fa-regular fa-copy"></i>
        </span>
        <span>Skill Assesment</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          <i class="fa-solid fa-clipboard-question"></i>
        </span>
        <span>Interview prep</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          {" "}
          <i class="fa-solid fa-file-lines"></i>
        </span>
        <span>Resume builder</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          <i className="fa-solid fa-play"></i>
        </span>
        <span>Job Seeker Guid</span>
      </div>
      <div className="d-flex gap-3 mt-4">
        <span>
          <i class="fa-solid fa-gear"></i>
        </span>
        <span>Job Seeker Guid</span>
      </div>
    </div>
  );
}

export default JobSideBar;
