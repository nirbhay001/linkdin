import React from "react";

function JobHeader() {
  return (
    <div
      className=" container-md d-flex flex-column  align-items-start rounded-2 border border-2 border-rgb(102,102,102) p-2 mt-3 "
      style={{ width: "40rem", height: "auto" }}
    >
      <span className="d-flex gap-3">
        <h5>Recommended for you</h5>
        <span style={{ marginLeft: "18rem" }}>clear</span>
      </span>
      <span className="font-weight-bold text-left">
        fronend Developer <span className="small text-primary"> new. 787</span>
      </span>
      <span className="small">Bengalure, karnatka, india</span>
      <div
        className="m-2"
        style={{ borderBottom: "1px solid #ccc", width: "37rem" }}
      />
      <span className="font-weight-bold text-left">
        Software Engineer <span className="small text-primary"> new. 787</span>
      </span>
      <span className="small">Bengalure, karnatka, india</span>
      <div
        className="m-2"
        style={{ borderBottom: "1px solid #ccc", width: "37rem" }}
      />
      <span className="font-weight-bold text-left">
        Remote <span className="small text-primary"> new. 787</span>
      </span>
      <span className="small">india</span>
    </div>
  );
}

export default JobHeader;
