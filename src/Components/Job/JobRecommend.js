import React from "react";
import { connect } from "react-redux";
import { useEffect } from "react";
import { jobUsers } from "../../Redux/Job/JobAction";
import { InfinitySpin } from "react-loader-spinner";

function JobRecommend(props) {
  useEffect(() => {
    props.jobUsers();
  }, []);

  return (
    <div
      className=" container-md d-flex flex-column rounded-2 border border-2 border-rgb(102,102,102) p-2 mt-3 "
      style={{ width: "40rem", height: "auto" }}
    >
      <h5>Job recommended for you</h5>
      <div className="small">Based on your profile and searches</div>
      {props.jobsData.status === "loading" ? (
        <div className="text-center">
          <InfinitySpin width="200" color="#4fa94d" />
        </div>
      ) : props.jobsData.status === "error" ? (
        <h1>Error happening in fetching the data</h1>
      ) : (
        props.jobsData.jobs.map((item) => {
          return (
            <div
              className="d-flex justify-content-between align-items-center flex-row p-3 mt-3"
              key={item.id}
            >
              <div className="d-flex justify-content-center gap-2">
                <img
                  src={item.imgUrl}
                  style={{ width: "4rem", height: "4rem" }}
                />
                <div className="d-flex flex-column gap-1">
                  <div className="font-weight-bold text-primary">
                    {item.job}
                  </div>
                  <span>{item.company}</span>
                  <span>{item.location}</span>
                </div>
              </div>
              <i class="fa-sharp fa-solid fa-bookmark fa-2xl "></i>
            </div>
          );
        })
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    jobsData: state.rootReducer.jobs,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    jobUsers: () => dispatch(jobUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(JobRecommend);
