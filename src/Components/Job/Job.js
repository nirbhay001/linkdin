import React from "react";
import JobSideBar from "./JobSideBar";
import JobHeader from "./JobHeader";
import RightSideBar from "./RightSideBar";
import JobRecommend from "./JobRecommend";

function Job() {
  return (
    <div className="d-flex job">
      <JobSideBar />
      <div className="d-flex flex-column">
        <JobHeader />
        <JobRecommend />
      </div>
      <RightSideBar />
    </div>
  );
}

export default Job;
