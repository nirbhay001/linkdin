import React from "react";

function RightSideBar() {
  return (
    <div
      className="d-flex flex-column rounded-2 border border-2 border-rgb(102,102,102)  m-3 p-3 d-none d-md-block "
      style={{ width: "300px", height: "40vh", position: "relative" }}
    >
      <div className="font-weight-bold text-left">Job seeker guidance</div>
      <span className="small">Recommended based on your activity</span>
      <div className="d-flex justify-content-between align-items-center bg-success mt-1 g-4 p-2">
        <div>I want to improve my resume</div>
        <img
          src={
            "https://thumbs.dreamstime.com/z/resume-icon-isolated-white-background-your-web-mobile-app-design-resume-icon-vector-sign-symbol-isolated-white-134162302.jpg"
          }
          style={{ width: "3rem", height: "3rem" }}
        ></img>
      </div>
      <span className="font-weight-light pt-2">
        Explore our curated guide of expert-led courses, such as how to improve
        your resume and grow your network, to help you land your next
        opportunity.
      </span>
    </div>
  );
}

export default RightSideBar;
