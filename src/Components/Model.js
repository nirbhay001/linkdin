import React from "react";
import { useState } from "react";
import { postData } from "../Redux/Home/HomeAction";
import { connect } from "react-redux";

function Model(props) {
  const [state, setState] = useState({
    name: "Abhinav",
    userName: "abhinav001",
    profession: "Software Engineer",
    company: "xyz corp",
    imgUrl: "www.example.com",
    postText: "",
    likes: 4,
    country: "india",
    comment: ["good"],
  });

  const handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    setState((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  const handleData = () => {
    props.postData(state);
  };
  return (
    <div>
      <div
        className="modal"
        id="myAccountModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myAccountModelLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <i class="fa-regular fa-circle-user fs-1"></i>
            </div>
            <select
              className="form-select form-select-sm "
              aria-label=".form-select-sm example"
              name="imgUrl"
              onChange={(event) => handleChange(event)}
              required
            >
              <option selected="">Open this select image url</option>
              <option value="https://picsum.photos/id/13/367/267">img1</option>
              <option value="https://picsum.photos/id/15/367/267">img2</option>
              <option value="https://picsum.photos/id/17/367/267">img3</option>
            </select>
            <div className="form-floating mt-4">
              <textarea
                className="form-control"
                placeholder="Leave a comment here"
                id="floatingTextarea"
                name="postText"
                required
                defaultValue={""}
                onChange={(event) => handleChange(event)}
              />
              <label htmlFor="floatingTextarea">Comments</label>
            </div>

            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={handleData}
                data-bs-dismiss="modal"
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    postData: (inputData) => dispatch(postData(inputData)),
  };
};

export default connect(null, mapDispatchToProps)(Model);
