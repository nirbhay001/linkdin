import React from "react";

function NotificationSidebar() {
  return (
    <div
      className="d-flex flex-column rounded-2 align-items-center justify-content-center border border-2 border-rgb(102,102,102)  m-3  d-none d-md-block "
      style={{ width: "250px", height: "20vh", position: "relative" }}
    >
      <div className="tect-bold p-3">Manage Your Notification</div>
      <div className="text-primary text-center">View Setting</div>
    </div>
  );
}

export default NotificationSidebar;
