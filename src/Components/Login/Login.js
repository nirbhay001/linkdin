import React from 'react'
import './Login.css'

function Login() {
  return (
    <div className="container mt-5">
    <div className="row justify-content-center">
      <div className="col-md-4">
        <form className="bg-white p-4 shadow rounded">
          <h1 className="mb-4">Sign In</h1>
          <div className="form-group">
            <input
              type="email"
              className="form-control"
              id="email"
              placeholder="Email or Phone"
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
            />
          </div>
          <button type="submit" className="btn btn-primary btn-block">
            Sign In
          </button>
          <hr />
          <p className="text-muted">
            Don't have an account? <a href="#">Join now</a>
          </p>
        </form>
      </div>
    </div>
  </div>
  )
}

export default Login

