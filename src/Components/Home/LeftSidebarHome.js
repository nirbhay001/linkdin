import React from "react";

function LeftSidebarHome() {
  return (
    <>
      <div
        className="d-flex flex-column col-2 rounded-2 border border-2 border-rgb(102,102,102) align-items-center m-3  d-none d-md-block "
        style={{ width: "200px", height: "50vh", position: "relative" }}
      >
        <div style={{ borderBottom: "4rem solid rgb(160,180,183)" }}></div>
        <div
          style={{
            position: "absolute",
            top: "2.5rem",
            left: "50%",
            transform: "translateX(-50%)",
          }}
        >
          <img
            src={"https://placeimg.com/400/400/male"}
            className="rounded-circle  py-n4 "
            style={{ width: "3rem" }}
          />
        </div>
        <h6 className="mt-5 p-2">Nirbhay Kumar Pandey</h6>
        <div className="p-2">
          B.tech in Eectronics and Communication Engineering
        </div>
        <hr />
        <div className="d-flex flex-column gap-2 p-2">
          <div className="d-flex justify-content-sm-between">
            <span className="d-flex flex-column align-items-start">
              <span> Connection</span>
              <span className="d-block small">grow your Network</span>
            </span>
            <span>96</span>
          </div>
          <div className="d-flex justify-content-sm-between  ml-n5">
            <span className="small">viewed your profile</span>
            <span className="d-block small">11</span>
          </div>
        </div>
      </div>
    </>
  );
}

export default LeftSidebarHome;
