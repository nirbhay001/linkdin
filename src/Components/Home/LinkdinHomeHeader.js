import React from "react";
import Model from "../Model";

function LinkdinHomeHeader() {
  return (
    <div
      className=" container-md d-flex flex-column  justify-contents-start rounded-2 border border-2 border-rgb(102,102,102) p-3 mt-3 "
      style={{ width: "40rem" }}
    >
      <div className="d-flex gap-2">
        <img
          src={
            "https://fastly.picsum.photos/id/12/2500/1667.jpg?hmac=Pe3284luVre9ZqNzv1jMFpLihFI6lwq7TPgMSsNXw2w"
          }
          style={{ width: "3rem", height: "3rem" }}
          className="rounded-circle"
          alt=""
          profile
          image
        />
        <input
          type="text"
          className="form-control rounded-5"
          placeholder="start a post"
          data-bs-toggle="modal"
          data-bs-target="#myAccountModal"
          readOnly
        ></input>
        <Model />
      </div>
      <div className="d-flex justify-content-between gap-3 mt-3">
        <span>
          <i className="fa-sharp fa-solid fa-image fa-xl text-primary"></i>
          <span className="p-2">Photo</span>
        </span>
        <span>
          <i className="fa-solid fa-play fa-xl text-success"></i>
          <span className="p-2">Video</span>
        </span>
        <span>
          <i class="fa-solid fa-calendar-days fa-xl text-warning"></i>
          <span className="p-2">event</span>
        </span>
        <span>
          <i class="fa-sharp fa-solid fa-newspaper fa-xl text-danger"></i>
          <span className="p-2">Write Article</span>
        </span>
      </div>
    </div>
  );
}

export default LinkdinHomeHeader;
