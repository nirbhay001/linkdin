import React from "react";
import { Link } from "react-router-dom";

function RightSidebarHome() {
  return (
    <div
      className="d-flex flex-column rounded-2 col-2 border border-2 border-rgb(102,102,102) m-3 d-none d-none d-lg-block"
      style={{ width: "300px", height: "50vh", position: "relative" }}
    >
      <div className="d-flex justify-content-between p-2">
        <span>Linkdin new's</span>
        <span
          className="text-white text-center"
          style={{
            backgroundColor: "black",
            width: "1.5rem",
            height: "1.5rem",
          }}
        >
          i
        </span>
      </div>
      <div>
        <div className="d-flex flex-column align-items-start  m-0 p-2">
          <div className="d-flex justify-content-start m-0">
            <span>
              <i className="fa-sharp fa-solid fa-circle fa-2xs"></i>
              <Link to="/news" ><span className="p-1">
                Here are news about 2023 top companies of india...
              </span>
              </Link>
            </span>
          </div>
          <div className="small">{"  "}3 Hour ago . 1741 member like this</div>
        </div>
        <div className="d-flex flex-column align-items-start gap-2 p-2">
          <div className="d-flex justify-content-start gap-2">
            <span>
              <i className="fa-sharp fa-solid fa-circle fa-2xs"></i>
              <Link to="/news">
              <span className="p-1">
                Here are news about 2023 top companies of America...
              </span>
              </Link>
            </span>
          </div>
          <div className="small">{"  "}4 Hour ago . 1842 member like this</div>
        </div>
        <div className="d-flex flex-column align-items-start gap-2 p-2">
          <div className="d-flex justify-content-start gap-2">
            <span>
              <i className="fa-sharp fa-solid fa-circle fa-2xs"></i>
              <Link to="./news">
              <span className="p-1">
                Here are news about 2023 top companies...
              </span>
              </Link>
            </span>
          </div>
          <div className="small">
            {"  "}1 Hour ago . 1842 member like this...
          </div>
        </div>
      </div>
    </div>
  );
}

export default RightSidebarHome;
