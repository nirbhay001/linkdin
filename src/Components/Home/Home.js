import React from "react";
import { connect } from "react-redux";
import LeftSidebarHome from "./LeftSidebarHome";
import RightSidebarHome from "./RightSidebarHome";
import LinkdinHomeHeader from "./LinkdinHomeHeader";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { fetchUsers } from "../../Redux/Home/HomeAction";
import { InfinitySpin } from "react-loader-spinner";
import { updateData } from "../../Redux/Home/HomeAction";

function Home(props) {
  const [like, setLike] = useState(false);
  const [commentFlagInput, setCommentFlagInput] = useState({
    comment: "",
  });
  const [commentFlag, setCommentFlag] = useState(false);

  useEffect(() => {
    props.fetchUsers();
  }, []);

  const handleClick = (item) => {
    if (like === true) {
      return;
    }
    let newItem = {
      ...item,
      likes: item.likes + 1,
    };
    props.updateData(newItem);
    setLike(true);
  };

  const handleCommentChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    setCommentFlagInput({
      [name]: value,
    });
  };

  const handleCommentClick = (item) => {
    let commentvalue = commentFlagInput.comment;
    let itemData = {
      ...item,
      comment: [...item.comment, commentvalue],
    };

    props.updateData(itemData);
  };

  const showComment = () => {
    setCommentFlag(!commentFlag);
  };

  return (
    <>
      <div className="container-sm col-6 d-flex justify-content-center home">
        <LeftSidebarHome />
        <div className="d-flex flex-column">
          <LinkdinHomeHeader />
          {props.usersData.status === "loading" ? (
            <div className="text-center">
              <InfinitySpin width="200" color="#4fa94d" />
            </div>
          ) : props.usersData.status === "error" ? (
            <h1 className="text-center">Error happened in fetching the data</h1>
          ) : (
            props.filterData.map((item) => {
              return (
                <div
                  className=" container-md d-flex flex-column justify-contents-start rounded-2 border border-2 border-rgb(102,102,102) p-3 mt-3 "
                  style={{ height: "auto" }}
                  key={item.id}
                >
                  <div className="d-flex flex-row gap-2">
                    <img
                      src={item.imgUrl}
                      className="rounded-circle "
                      alt="Profile Picture"
                      style={{ width: "3rem ", height: "3rem" }}
                    />
                    <div>
                      <span className="d-flex ">
                        <Link to={`/user/${item.id}`}>
                          <span className="font-weight-bold">{item.name}</span>
                        </Link>
                        <span className="font-weight-bold pl-2 ">.</span>
                        <span className="pl-5">3rd+</span>
                      </span>
                      <span>
                        <span>
                          || {item.profession} || {item.company}...
                        </span>
                      </span>
                    </div>
                  </div>
                  <div className="mt-2">{item.postText}</div>
                  <img
                    src={item.imgUrl}
                    className=" img-fluid w-100"
                    alt="Profile Picture"
                    style={{ width: "40rem" }}
                  />
                  <div className="user-like fw-bolder">
                    {item.likes}
                    <img
                      src={
                        "https://www.userflow.nl/images/Linkedin-Celebrate-Icon-ClappingHands500.png"
                      }
                      style={{
                        width: "1.5rem",
                        height: "1.5rem",
                      }}
                    />
                  </div>
                  <hr />
                  <div className="d-flex justify-content-around">
                    <span className="hover-element">
                      <i className="fa-solid fa-thumbs-up"></i>
                      <span onClick={() => handleClick(item)}>Like</span>
                    </span>
                    <span className="hover-element" onClick={showComment}>
                      <i className="fa-solid fa-comment"></i>
                      <span>Comment</span>
                    </span>
                    <span className="hover-element">
                      <i className="fa-solid fa-share"></i>
                      <span>Repost</span>
                    </span>
                    <span className="hover-element">
                      <i className="fa-solid fa-share-nodes"></i>
                      <span>Send</span>
                    </span>
                  </div>
                  {commentFlag === true ? (
                    <div>
                      <hr />
                      <input
                        type="text"
                        className="form-control rounded-5"
                        placeholder="Add a comment"
                        name="comment"
                        onChange={handleCommentChange}
                      ></input>
                      <button
                        type="button"
                        class="btn btn-primary btn-sm m-1"
                        onClick={() => handleCommentClick(item)}
                      >
                        send
                      </button>
                      {item.comment.map((commentData) => {
                        return <p className="p-3">{commentData}</p>;
                      })}
                      <div></div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              );
            })
          )}
        </div>
        <RightSidebarHome />
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  console.log(state.rootReducer.user.filterData);
  return {
    usersData: state.rootReducer.user,
    filterData: state.rootReducer.user.filterData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
    updateData: (item) => dispatch(updateData(item)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
