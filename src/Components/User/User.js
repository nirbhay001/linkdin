import React from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";

function User(props) {
  let { userdata } = useParams();
  let user = props.usersData.filter((item) => {
    return item.id ===userdata;
  })[0];
  return user === undefined ? (
    <h1 className="text-center">Go back to home page</h1>
  ) : (
    <div
      className="d-flex flex-column justify-content-start rounded-2 border border-2 border-rgb(102,102,102)  m-3 w-50 mx-auto "
      style={{ height: "30rem", position: "relative" }}
    >
      <div style={{ borderBottom: "4rem solid rgb(160,180,183)" }}></div>
      <div
        style={{
          position: "absolute",
          top: "1.5rem",
          left: "15%",
          transform: "translateX(-50%)",
        }}
      >
        <img
          src={user.imgUrl}
          className="rounded-circle  py-n4 "
          style={{ width: "6rem", height: "6rem" }}
          alt="user image"
        />
      </div>
      <div className="d-flex">
        <h3 className="mt-5 p-2" style={{ marginTop: "2rem" }}>
          {user.name}
        </h3>
        <span className="mt-5 p-3"> 1st</span>
      </div>
      <h5 className="ms-sm-3">{user.company}</h5>
      <span className="ms-sm-3">{user.postText}</span>
      <div className="ms-sm-3">
        Banglore, India <span className="text-primary"> Contact Info</span>
      </div>
      <div className="ms-sm-3">
        232 follower . <span className="text-primary">500 + connections</span>
      </div>
      <div className="ms-sm-3">
        <img
          src={
            "https://unsplash.com/assets/api/applications/figma-be8e1549471b570baa86bdde85bda18f65776f40b16dab4facdfbf1c9ced8f7d.jpg"
          }
          className="rounded-circle"
          style={{ width: "1.5rem", height: "1.5rem" }}
        />{" "}
        Ravi and many people like this.
      </div>
      <div className="d-flex p-3 gap-3 rounded-3">
        <button type="button" className="btn btn-primary btn-pill">
          Message
        </button>
        <button type="button" className="btn btn-outline-secondary btn-pill">
          More
        </button>
      </div>
      <div className="dropdown"></div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    usersData: state.rootReducer.user.users,
  };
};
export default connect(mapStateToProps, null)(User);
