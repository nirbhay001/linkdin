import React from "react";
import NotificationSidebar from "./NotificationSidebar";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { fetchnotifications } from "../Redux/Notification/NotificationAction";
import { InfinitySpin } from "react-loader-spinner";

function Notification(props) {
  useEffect(() => {
    props.fetchnotifications();
  }, []);

  return (
    <div className="container d-flex justify-content-center rounded notification">
      <NotificationSidebar />
      <div className="container d-flex flex-column  justify-contents-center border border-2 border-rgb(102,102,102)  mt-3">
        {props.notificationData.status === "loading" ? (
          <div className="text-center">
            <InfinitySpin width="200" color="#4fa94d" />
          </div>
        ) : props.notificationData.status === "error" ? (
          <h1>Error in fetching the data</h1>
        ) : (
          props.notificationData.notifications.map((item) => {
            return (
              <div
                className="d-flex justify-content-between hover-element align-items-center gap-3 p-4  notification-item"
                style={{ width: "40rem" }}
                key={item.id}
              >
                <div className="d-flex gap-2">
                  <img
                    src={item.imgUrl}
                    style={{ width: "3rem", height: "3rem" }}
                  />
                  <div>
                    <Link to={`/user/${item.id}`}>
                      <span className="font-weight-bold">{item.name}</span>
                    </Link>
                    <span className="p-1">{item.postText}</span>
                  </div>
                </div>
                <div className="d-flex flex-column justify-content-center">
                  <span>3h</span>
                  <span>...</span>
                </div>
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    notificationData: state.rootReducer.notification,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchnotifications: () => dispatch(fetchnotifications()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
