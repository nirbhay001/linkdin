import React from "react";

function News() {
  return (
    <div className="container-sm border col-6 d-flex justify-content-center home mt-5">
      <div style={{ borderBottom: "10rem solid rgb(160,180,183)" }}></div>
      <div
        style={{
          position: "absolute",
          top: "8rem",
        }}
      >
        <img
          src={"https://placeimg.com/400/400/male"}
          className="py-n4 "
          style={{ width: "50vw", height: "20rem" }}
        />
      </div>
      <div style={{ marginTop: "20rem" }}>
        <div
          className="border m-auto p-3"
          style={{
            backgroundColor: "white",
            width: "40vw",
            transform: "translateY(-5rem)",
          }}
        >
          <h4>Finance emerges as a top employer </h4>
          <span className="text-primary">
            By Preethi Ramamoorthy, Editor at LinkedIn News
          </span>
          <div className="text-secondary">updated 2 days ago</div>
          <p>
            Ten of the top 25 companies on this year's Top Companies list belong
            to this space, including Morgan Stanley, Macquarie Group, Deloitte,
            HDFC Bank, Mastercard, and Yubi.
          </p>
          <div className="border border-2 rounded p-3">
            <div className="d-flex gap-3 p-3">
              <img
                src={"https://placeimg.com/400/400/male"}
                style={{ width: "3rem", height: "3rem" }}
              />
              <div className="d-flex flex-column">
                <div>Linkedin News</div>
                <div className="text-secondary">56,300 follower</div>
              </div>
            </div>
            <div>
              Firms from the financial services, banking, and fintech sectors
              dominate LinkedIn’s Top Companies list in India this year. In
              fact, 10 of the 25 companies belong to this space, including
              Morgan Stanley, Macquarie Group, Deloitte, HDFC Bank, Mastercard,
              and Yubi. Experts say this reflects the role finance companies
              play in an emerging market like India. “Financial services act as
              a booster for economic growth and international trade,” says
              Partheenban Ezhil, Audit Senior Assurance at EY. “It promotes
              savings and investments. Overall, it helps in economic development
              by satisfying financial needs.” But what makes companies in the
              finance sector a top workplace for professionals? Ezhil says its
              complex nature and huge scope of work means there is a perennial
              demand for professionals with expertise. Companies in this sector
              are stepping up their hiring for the next financial year with an
              increase in demand for talent in housing finance, private wealth,
              technology, lending, and compliance, says a report by The Economic
              Times. Finance companies are also doubling down on their efforts
              to enhance employee experience.
              
               Providing employees with
              opportunities for personal growth is a key priority. In a
              statement released by Morgan Stanley on LinkedIn, Mandell Crawley,
              CHRO says, “It is critical that our people have professional and
              leadership development programmes, formal mentorship experiences,
              and networking opportunities at their fingertips to grow and
              advance in their careers.”
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default News;
