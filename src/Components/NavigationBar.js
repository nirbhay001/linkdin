import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { FILTER_DATA } from "../Redux/Home/HomeType";

function NavigationBar(props) {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light row">
      <div className="container d-flex flex-nowrap justify-content-around align-items-center">
        <div className="d-flex">
          <a className="navbar-brand d-flex align-items-center gap-1 " href="#">
            <i
              className="fa-brands fa-linkedin fa-xl "
              style={{ color: "#0457e7" }}
            />
            <span className="d-none d-md-block ">Linkdin</span>
          </a>
          <form className="form-inline my-2 my-lg-0 mr-3">
            <input
              className="form-control mr-sm-2 w-100 !important e-5"
              type="search"
              placeholder="Search"
              aria-label="Search"
              onChange={(event) => props.filterResult(event.target.value)}
            />
          </form>
        </div>
        <ul className="navbar-nav list-unstyled d-flex flex-row flex-nowrap justify-content-around align-items-center mt-3 s-3 gap-3">
          <li className="nav-item active">
            <Link
              to="/"
              className="d-flex flex-column align-items-center"
              style={{ textDecoration: "none", color: "black" }}
            >
              <i className="fa-solid fa-house fa-lg  mx-sm-2" />

              <span className="d-none d-md-block">
                <a className="nav-link" href="#">
                  Home <span className="sr-only">(current)</span>
                </a>
              </span>
            </Link>
          </li>
          <li className="nav-item active ">
            <Link
              to="/mynetwork"
              className="d-flex flex-column align-items-center"
              style={{ textDecoration: "none", color: "black" }}
            >
              <i className="fa-solid fa-user-group fa-lg mx-sm-2"></i>

              <span className="d-none d-md-block">
                <a className="nav-link " href="#">
                  My Network
                </a>
              </span>
            </Link>
          </li>
          <li className="nav-item active ">
            <Link
              to="/job"
              className="d-flex flex-column justify-content-center align-items-center"
              style={{ textDecoration: "none", color: "black" }}
            >
              <i className="fa-sharp fa-solid fa-briefcase fa-lg mx-sm-2"></i>
              <span className="d-none d-md-block">
                <a className="nav-link" href="#">
                  Jobs
                </a>
              </span>
            </Link>
          </li>
          <li className="nav-item active ">
            <Link
              to="/message"
              className="d-flex flex-column justify-content-center align-items-center"
              style={{ textDecoration: "none", color: "black" }}
            >
              <i className="fa-solid fa-message fa-lg mx-sm-2" />

              <span className="d-none d-md-block">
                <a className="nav-link" href="#">
                  Messaging
                </a>
              </span>
            </Link>
          </li>
          <li className="nav-item active ">
            <Link
              to="/notification"
              className="d-flex flex-column justify-content-center align-items-center"
              style={{ textDecoration: "none", color: "black" }}
            >
              <i className="fas fa-bell fa-lg mx-sm-2" />
              <span className="d-none d-md-block">
                <a className="nav-link" href="#">
                  Notifications
                </a>
              </span>
            </Link>
          </li>
          <li className="nav-item active d-flex flex-column justify-content-start align-items-center">
            <img
              src={"https://picsum.photos/id/20/367/267"}
              className="rounded-circle  py-n4 navbar-style "
              style={{ width: "1.5rem", height: "1.5rem" }}
            />
            <a
              className="nav-link dropdown-toggle navbar-style"
              href="#"
              id="navbarDropdownMenuLink"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Me
            </a>
            <div
              className="dropdown-menu"
              aria-labelledby="navbarDropdownMenuLink"
            >
              <a className="dropdown-item" href="#">
                Profile
              </a>
              <a className="dropdown-item" href="#">
                Settings
              </a>
              <a className="dropdown-item" href="#">
                Logout
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    filterResult: (value) =>
      dispatch({
        type: FILTER_DATA,
        payload: value,
      }),
  };
};

export default connect(null, mapDispatchToProps)(NavigationBar);
